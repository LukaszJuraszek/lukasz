<?php
/**
 * Created by PhpStorm.
 * User: john5
 * Date: 2016-04-03
 * Time: 15:49
 */

namespace Lukasz\Car;

require 'Car.php';

class ClassicCar extends Car
{

    /**
     * ClassicCar constructor.
     */
    public function __construct()
    {
        $this->car_type = $this->available_car_types[0];
    }
}