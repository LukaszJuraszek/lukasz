<?php
/**
 * Created by PhpStorm.
 * User: john5
 * Date: 2016-04-03
 * Time: 15:44
 */

namespace Lukasz\Car;

require 'CarInterface.php';


abstract class Car implements CarInterface
{
    protected $available_car_types = ['Car', 'Truck'];
    
    public $car_type;

    /**
     * @return mixed
     */
    public function getCarType()
    {
        return $this->car_type;
    }
}