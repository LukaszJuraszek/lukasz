<?php
/**
 * Created by PhpStorm.
 * User: john5
 * Date: 2016-04-03
 * Time: 15:56
 */



namespace Lukasz\Parking;

use Lukasz\Car\Car;


class Parking
{
    
    private $number_of_car_places;
    
    private $number_of_truck_places;

    private $car_type_which_wants_to_park;

    /**
     * @return mixed
     */
    public function getNumberOfCarPlaces()
    {
        return $this->number_of_car_places;
    }

    /**
     * @param mixed $number_of_car_places
     */
    public function setNumberOfCarPlaces($number_of_car_places)
    {
        $this->number_of_car_places = $number_of_car_places;
    }

    /**
     * @return mixed
     */
    public function getNumberOfTruckPlaces()
    {
        return $this->number_of_truck_places;
    }

    /**
     * @param mixed $number_of_truck_places
     */
    public function setNumberOfTruckPlaces($number_of_truck_places)
    {
        $this->number_of_truck_places = $number_of_truck_places;
    }

    /**
     * @param Car $car
     */
    public function setCarType(Car $car)
    {
        $this->car_type_which_wants_to_park = $car->getCarType();
    }


    /**
     * @return string
     */
    public function parkTheCar()
    {
        if($this->car_type_which_wants_to_park === 'Car')
        {
            if($this->number_of_car_places > 0)
            {
                $this->number_of_car_places--;

                return 'Zaparkowano samochód';
            }
            else
            {
                return 'Brak wolnych miejsc';
            }

        }
        elseif($this->car_type_which_wants_to_park === 'Truck')
        {
            if($this->number_of_truck_places > 0)
            {
                $this->number_of_truck_places--;

                return 'Zaparkowano samochod';
            }
            else
            {
                return 'Brak wolnych miejsc';
            }
        }
    }

    /**
     * @return mixed
     */
    public function getCarTypeWhichWantsToPark()
    {
        return $this->car_type_which_wants_to_park;
    }

}